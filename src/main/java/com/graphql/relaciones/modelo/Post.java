package com.graphql.relaciones.modelo;

import lombok.Data;

@Data
public class Post {

	private Long idPost;
	private String comment;
	
}
