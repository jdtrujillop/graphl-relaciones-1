package com.graphql.relaciones.modelo;

import java.util.List;

import lombok.Data;

@Data
public class User {

	private Long idUser;
	private String name;
	private List<Post> posts;
	
}
