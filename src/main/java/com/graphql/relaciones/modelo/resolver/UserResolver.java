package com.graphql.relaciones.modelo.resolver;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.graphql.relaciones.modelo.Post;
import com.graphql.relaciones.modelo.User;
import com.graphql.relaciones.servicio.especificacion.PostService;

@Component
public class UserResolver implements GraphQLResolver<User> {
	
	@Autowired
	private PostService postService; 
	
	public List<Post> getPosts(User user){
		return postService.getPostByIdUser(user.getIdUser());
	} 

}
