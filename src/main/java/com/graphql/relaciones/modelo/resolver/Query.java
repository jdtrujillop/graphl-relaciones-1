package com.graphql.relaciones.modelo.resolver;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.graphql.relaciones.modelo.User;
import com.graphql.relaciones.servicio.especificacion.UserService;

@Component
public class Query implements GraphQLQueryResolver {
	
	@Autowired
	private UserService userService;
	
	public List<User> allUsers(){		
		return userService.getAllUsers();
	}
	
	public User user(Long idUser) {
		return null;
	}
	
	public Long countUsers() {
		return 0L;
	}

}
