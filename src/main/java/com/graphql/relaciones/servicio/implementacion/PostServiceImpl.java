package com.graphql.relaciones.servicio.implementacion;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.graphql.relaciones.modelo.Post;
import com.graphql.relaciones.servicio.especificacion.PostService;

@Service
public class PostServiceImpl implements PostService{

	@Override
	public List<Post> getPostByIdUser(Long idUser) {

		System.out.println("Aca 2");
		Post post = new Post();
		post.setComment("Mi primero comentario 2");
		post.setIdPost(1L);
		
		List<Post> listPost = new ArrayList<>();
		listPost.add(post);
		
		return listPost;
	}
 
}
