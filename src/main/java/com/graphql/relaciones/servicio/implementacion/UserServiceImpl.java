package com.graphql.relaciones.servicio.implementacion;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.graphql.relaciones.modelo.User;
import com.graphql.relaciones.servicio.especificacion.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Override
	public List<User> getAllUsers() {
		
		List<User> listUser = new ArrayList<>();
		User user = new User();
		user.setIdUser(1L);
		user.setName("Juan Diego");
		listUser.add(user);
		
		System.out.println("Aca 1");
		
		return listUser;
	}

}
