package com.graphql.relaciones.servicio.especificacion;

import java.util.List;

import com.graphql.relaciones.modelo.Post;

public interface PostService {
	
	public List<Post> getPostByIdUser(Long idUser);

}
