package com.graphql.relaciones.servicio.especificacion;

import java.util.List;

import com.graphql.relaciones.modelo.User;

public interface UserService {

	public List<User> getAllUsers();
}
